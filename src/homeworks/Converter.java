package homeworks;

import java.util.Scanner;

public class Converter {

	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter EUR amount");
		double EURinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter USD amount");
		double USDinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Meters amount");
		double Metersinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Feet amount");
		double Feetinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Celsius amount");
		double Celsiusinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Fahrenheit amount");
		double Fahrenheitinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Kelvin amount");
		double Kelvininput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Square meters amount");
		double Squareminput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Square kilometers amount");
		double Squarekminput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Cubic meters amount");
		double Cubicminput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Liters amount");
		double Literinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Kilograms amount");
		double Kginput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Pounds amount");
		double Poundinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Hours amount");
		double Hourinput = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Days amount");
		double Dayinput = Double.parseDouble(myScanner.nextLine());
		myScanner.close();
		
		double USD = EURinput * 1.22;
		double EUR = USDinput * 0.82;
		double Feet = Metersinput * 3.28084;
		double Meters = Feetinput / 3.28084;
		double Fahrenheit = (Celsiusinput * 9/5) + 32;
		double Kelvin = Celsiusinput + 273.15;
		double CelsiusK = Kelvininput - 273.15; 
		double CelsiusF = (Fahrenheitinput - 32) * 5/9;
		double Squarekm = Squareminput / 1000000;
		double Squarem = Squarekminput * 1000000;
		double Liter = Cubicminput * 1000;
		double Cubicm = Literinput / 1000;
		double Pound = Kginput * 2.20462;
		double Kg = Poundinput / 2.20462;
		double Day = Hourinput / 24;
		double Hour = Dayinput * 24;
		
		
		System.out.println("USD: " + USD);
		System.out.println("EUR: " + EUR);
		System.out.println("Feet: " + Feet);
		System.out.println("Meters: " + Meters);
		System.out.println("Fahrenheit: " + Fahrenheit);
		System.out.println("Kelvin: " + Kelvin);
		System.out.println("Celsius from Fahrenheit: " + CelsiusF);
		System.out.println("Celsius from Kelvin: " + CelsiusK);
		System.out.println("Square km: " + Squarekm);
		System.out.println("Square m: " + Squarem);
		System.out.println("Liters: " + Liter);
		System.out.println("Cubic m: " + Cubicm);
		System.out.println("Pounds: " + Pound);
		System.out.println("Kilograms: " + Kg);
		System.out.println("Days: " + Day);
		System.out.println("Hours: " + Hour);
			

		
	}
}


