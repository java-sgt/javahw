package homeworks;

import java.util.Scanner;

public class CalculatorHW {

	public static void main(String[] args) {
		String operatorStr = "";
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter the first number");	
		double number1 = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter the operator +, -, /, *, %, p, b or s");
		operatorStr = myScanner.nextLine();
		
		System.out.println("Enter the second number");
		double number2 = myScanner.nextDouble();
		myScanner.close();
		
		char operator = operatorStr.charAt(0);
		double result = 0;
		
		switch (operator) {
		case '+':
			result = number1+number2;
			System.out.println(result);
			break;
		case '-':
			result = number1 - number2;
			System.out.println(result);
			break;
		case '/':
			result = number1 / number2;
			System.out.println(result);
			break;
		case '*':
			result = number1 * number2;
			System.out.println(result);
			break;
		case '%':
			result = number1 % number2;
			System.out.println(result);
			break;
		case 'p':
			System.out.println(number1 + ", " + number2);
			break;
		case 'b':
			if (number1 > number2)
				System.out.println(number1 + " is bigger than " + number2);
			else if (number2 > number1)
				System.out.println(number2 + " is bigger than " + number1);
			else
				System.out.println(number1 + " and " + number2 + " are equal");
			break;
		case 's':
			if (number1 > number2)
				System.out.println(number2 + " is smaller than " + number1);
			else if (number2 > number1)
				System.out.println(number1 + " is smaller than " + number2);
			else
				System.out.println(number1 + " and " + number2 + " are equal");
			break;
			default:
				System.out.println("The operator is incorrect");
				break;
			
		}
		

	}

}
