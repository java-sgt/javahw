package homeworks;

import java.util.Scanner;

public class DayType {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the day number in the week");
		int dayOfWeek = Integer.parseInt(myScanner.nextLine());
		myScanner.close();
		
		switch (dayOfWeek) {
		case 1, 2, 3, 4, 5:
			System.out.println("It is a working day");
			break;
		case 6, 7:
			System.out.println("It is a holiday");
		break;
		default:
			System.out.println("Day number is incorrect!");
			break;
		}

	}

}
