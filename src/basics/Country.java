package basics;

import java.util.*;
//or import java.util.Scanner   * menas we can use all util classes

public class Country {
	
	public static String countryName;
	public static String capitalCity;
	public static int population;
	public static boolean isInEU;
	public static double GDP;
	//public static String varString = new String("Text Value");
	//public static String varString2 = "Text value"; //2 different approaches
	
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter Country name");
		Country.countryName = myScanner.nextLine();
		
		System.out.println("Enter Capital");
		Country.capitalCity = myScanner.nextLine();
		
		System.out.println("Enter Population");
		Country.population = Integer.parseInt(myScanner.nextLine());
		
		System.out.println("Is the Country in EU?");
		Country.isInEU = Boolean.parseBoolean(myScanner.nextLine());
		
		System.out.println("Enter GDP:");
		Country.GDP = Double.parseDouble(myScanner.nextLine());
		myScanner.close();
		show();

	}
	
	public static void show() {
		System.out.println("Country Name: " + Country.countryName);
		System.out.println("Capital City: " + Country.capitalCity);
		System.out.println("Population: " + Country.population);
		System.out.println("Country in EU: " + Country.isInEU);
		System.out.println("GDP: " + Country.GDP);
		
		//in output you should enter all the values in Console and they will be shown
		
	}

}
