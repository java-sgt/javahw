package basics; //Homework nr1

import java.util.Scanner;

public class Company {
	
	public static String companyName;
	public static String headquarters;
	public static int employees;
	public static boolean listedInNASDAQ;
	public static int turnover;

	
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter Company name");
		Company.companyName = myScanner.nextLine();
		
		System.out.println("Enter headquarters location");
		Company.headquarters = myScanner.nextLine();
		
		System.out.println("Enter employee count");
		Company.employees = Integer.parseInt(myScanner.nextLine());
		
		System.out.println("Is the Company listed in NASDAQ?");
		Company.listedInNASDAQ = Boolean.parseBoolean(myScanner.nextLine());
		
		System.out.println("Enter GDP:");
		Company.turnover = Integer.parseInt(myScanner.nextLine());
		myScanner.close();
		show();

	}
	
	public static void show() {
		System.out.println("Company Name: " + Company.companyName);
		System.out.println("Headquarters location: " + Company.headquarters);
		System.out.println("Employee count: " + Company.employees);
		System.out.println("Company listed in NASDAQ: " + Company.listedInNASDAQ);
		System.out.println("Turnover: " + Company.turnover);

	}

}
