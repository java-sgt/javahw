package oopHW2;

import java.util.ArrayList;

public class District {

	private String title, city;
	private int districtID;

	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();
	private ArrayList<Person> personsInTheDistrict = new ArrayList<Person>();

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;
		this.city = city;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "The title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.personsInTheDistrict.size() + " persons in the district";
	}

	public void addOfficer(Officer officer) {
		this.officersInTheDistrict.add(officer);
	}

	public void removeOfficer(Officer officer) {
		this.officersInTheDistrict.remove(officer);
	}

	public void addPerson(Person person) {
		this.personsInTheDistrict.add(person);
	}

	public void removePerson(Person person) {
		this.personsInTheDistrict.remove(person);
	}

	public float calculateAvgLevellInDistrict() {
		float levelSum = 0;
		/// method size will return the number of the elements in the collection
		for (int i = 0; i < this.officersInTheDistrict.size(); i++)
			levelSum += this.officersInTheDistrict.get(i).calculateLevel();

		return levelSum / this.officersInTheDistrict.size();
	}
	

	public ArrayList<Officer> getOfficersInTheDistrict() {
		return officersInTheDistrict;
	}

	public void setOfficersInTheDistrict(ArrayList<Officer> officersInTheDistrict) {
		this.officersInTheDistrict = officersInTheDistrict;
	}

	public ArrayList<Person> getPersonsInTheDistrict() {
		return personsInTheDistrict;
	}

	public void setPersonsInTheDistrict(ArrayList<Person> personsInTheDistrict) {
		this.personsInTheDistrict = personsInTheDistrict;
	}

	public void addLawyer(Lawyer lawyer1) {
		// TODO Auto-generated method stub

	}

}
