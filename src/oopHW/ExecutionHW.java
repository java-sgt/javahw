package oopHW;

import java.util.ArrayList;
import java.util.Iterator;


public class ExecutionHW {

	public static void main(String[] args) {
		Officer[] officers = new Officer[7];
		
		officers[0] = new Officer("Tom", "Hanks", "District1", 190, 15);
		officers[1] = new Officer("John", "White", "District1", 231, 2);
		officers[2] = new Officer("Jane", "Snow", "District1", 246, 2);
		officers[3] = new Officer("Amy", "Sparkle", "District2", 261, 4);
		officers[4] = new Officer("Bob", "Smith", "District2", 231, 1);
		officers[5] = new Officer("Robert", "Wattson", "District2", 260, 5);
		officers[6] = new Officer("Emily", "Black", "District2", 213, 3);
		
		District district1 = new District("District1", "New-York", 12);
		District district2 = new District("District2", "Washington", 22);
		
		for (int i = 0; i < 3; i++)
			district1.addOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addOfficer(officers[i]);	
		
		
		Lawyer lawyer1 = new Lawyer("Bil", "Gates", 443, 2);
		Lawyer lawyer2 = new Lawyer("Will", "Smith", 313, 6);
		Lawyer lawyer3 = new Lawyer("Cameron", "Diaz", 505, 9);
		
		ArrayList<Lawyer> lawyers = new ArrayList<Lawyer>();
		
		lawyers.add(lawyer1);
		lawyers.add(lawyer2);
		lawyers.add(lawyer3);
		
		System.out.println("Information about Districts:");
		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());
		
		System.out.println("Information about Lawyers:");
		System.out.println(lawyer1);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer2);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());
		
		int crimeslawyers = lawyer1.getHelpedInCrimesSolving() + lawyer2.getHelpedInCrimesSolving() + lawyer3.getHelpedInCrimesSolving();
		
		System.out.println("The number of crimes in which solving the Lawyers were involved is " + crimeslawyers + ".");
		
		Lawyer bestLawyer = null;
		Iterator<Lawyer> iterator = lawyers.iterator();
		while (iterator.hasNext()) {
			Lawyer currentLawyer = iterator.next();
			if (bestLawyer == null || bestLawyer.getHelpedInCrimesSolving() < currentLawyer.getHelpedInCrimesSolving())
				bestLawyer = currentLawyer;
		}
		
		System.out.println(bestLawyer.getName() + " helped the most to solve crimes.");

	}

}
