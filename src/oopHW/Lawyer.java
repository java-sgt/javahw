package oopHW;


public class Lawyer extends Person {

	
	private int lawyerID, helpedInCrimesSolving;
	
	public Lawyer() {
		// TODO Auto-generated constructor stub
	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
		super.name = name;
		super.surname = surname;
		this.setHelpedInCrimesSolving(helpedInCrimesSolving);
		this.lawyerID = lawyerID;
	}


	public int getlawyerID() {
		return lawyerID;
	}

	public void setlawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	@Override
	public String toString() {
		return "Name: " + super.name + System.lineSeparator() + "Surname: " + super.surname + System.lineSeparator()
				+ "Lawyer ID: " + this.lawyerID + System.lineSeparator() + "Helped in crimes solving: "
				+ this.getHelpedInCrimesSolving();
	}
	


}
